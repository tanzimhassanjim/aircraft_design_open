clear global
clear all
close all
% test git
%__________________ Aircraft Design input_________________________________

take_off_dist=1000;          % Length of the Runway
Stall_velocity=60;           % Stall velocity in km/h
Cruise_velocity=150;         % Cruise velocity in km/h
Loiter_velocity=80;          % loiter velocity in km/h
Maximum_velocity=200;        % Maximum velocity in km/h 
density=0.00237;             % Sea level density in slug/ft^3
h_cruise=8000;              % Cruise height in ft
h_loiter=1500;               % Loiter height in ft
h_max_velocity=8000;        % Max velocity height in ft
hp_w=0.1;                    % Horse power to weight ratio  0.06-.2      

cl_max_cr= 2;                % Root Airfoil cl max
cl_max_ct= 2;                % Tip airfoil cl max


Swet_Sref=4;                 % Ratio between wetted area and referance area
Cfe=0.0055;                  % Skin friction co efficient 
e1=0.88;                     % Span Efficiency factor
static_margin=10;            % Static margin in percentage

flp_dfl_land=25;             % Flap diflection during landing
flp_dfl_takeoff=20;          % Flap diflection during takeoff

h_o=0;                       % Obasticle height
a_angle=3;                   % Approach angle of aircraft
landing_dist=1000;            % Landing distance in feet


%__________________________ Aircrft Geometry input________________________


weight_aircraft=50;         % weight of the aircraft in pound
ref_point=2;                % Distacne from aircraft nose to wing LE in ft
fine_ness_ratio=8;          % ratio of lingth Vs width

nose=.5;                    % Lenght of the nose in ft
AR_wing=10;                 % wing apsect ratio
AR_HT=3;                    % H-tail aspect ratio 2-5
AR_VT=1.5;                  % V-tail aspect ratio 1-3
TR=.4;                      % wing tapper ratio
TR_HT=1;                    % H-tail tapper ratio
TR_VT=1;                    % V-tail tapper ratio
arm=4;                      % Tail arm in ft
angle=0;                    % c/4 wing Sweep angle in degree
angle_ht=0;                 % c/4 H-tail sweep angle in degree
angle_vt=0;                 % c/4 V-tail sweep angle in degree
dihy_angle=0;               % wing dihydral angle in degree
V_HT=0.7;                   % H-tail volume coefficient 0.3-0.7
V_VT=0.04;                  % V-tail volume coefficient 0.02-.07
chord_flp =25;              % Flap '20-25 '% of wing chord
chord_aileron=20;           % Aileron '15-20 '% of wing chord
spn_of_flp=45;              % span of flap % of half wing span 40-50% ,if no aileron then 85%
spn_of_aileron=35;          % span of Aileron % of half wing span 30-35 %
spn_of_elevator=75;         % span of Elevator % of H tail 70- 75 %
chord_ele =30;              % Elevatro '25-30'% of elevator chord
chord_rud =30;              % Ruddder '25-30'% of Rudder chord


% ____________________________Default Value ______________________________

g_ground=32.2;              % Gravitational Accleration in ft/s^2
down_wash=0.4;              % Down-wash angle
a_o=0.0769*57.296;          % Lift slop of airfoil for max condition
alfa_max=11;                % angle at which cl max
alfa_zero=-10;              % angle at which lift =0
cd_max_cr=0.035;
cd_max_ct=0.035;
s_angle=1;                  % For stright wing s_angle=1 and for swept wing s_angle=0
Ao_w=0.11;                  % Lift curve slop for infinit wing
Ao_ht=0.095;                % Lift curve slop for infinit stabilizer
Ns=0.6;                     % Stabilizer efficiency 
J=1.15;
N=1;
mue=0.4;

%_________________ Calculations for aircraft design_______________________

% Determinig CL_max

% landing condition

cl_max_avg=(cl_max_cr+cl_max_ct)/2;             % Avarage cl 
dl_cl_max_landing= flp_dfl_land*(2/100);        % cl dew to flap diflection
cl_max_landing=cl_max_avg+dl_cl_max_landing;    % max cl     
CL_wing_landing=0.9*cl_max_landing;             % Wing max CL

% Take off condition

dl_cl_max_take_off=flp_dfl_takeoff*(2/100);
cl_max_take_off=cl_max_avg+dl_cl_max_take_off;        % max cl     
CL_wing_take_off=0.9*cl_max_take_off;                % Wing max CL

% Turning Condition

a_stright_wing=((a_o/(1+(a_o/(3.1416*e1*AR_wing)))));
a_swept_wing=((a_o*cosd(angle))/((1+((a_o*cosd(angle))/(3.1416*AR_wing))^2)^0.5)+((a_o*cosd(angle))/(3.1416*AR_wing)));

a=(a_stright_wing*sind(asind(s_angle))+a_swept_wing*(sind(acosd(s_angle))))*(3.1416/180);
CL_wing_turn=a*(alfa_max-alfa_zero);

CD_induse_max=((CL_wing_turn)^2)/(3.1416*e1*AR_wing);
CD_max=cd_max_cr+CD_induse_max;

L_D_max_turn=CL_wing_turn/CD_max;


%_____________ Density for cruise altitude____________________________

H=h_cruise/3.28;
P_SL=101325;
T_SL=288.15;
g=9.80665;
L=0.0065;
M_density=0.0289645;
R_earth=6400000;
R=8.31447;

T=T_SL-L*H;
g_h=g*(R_earth/(R_earth+H))^2;
P=P_SL*(1-(L*H)/T_SL)^((g_h*M_density)/(R*L));
density_cruise=((P*M_density)/(R*T))*.00194;

%________________Density for loiter altitude______________________________


H=h_loiter/3.28;
P_SL=101325;
T_SL=288.15;
g=9.80665;
L=0.0065;
M_density=0.0289645;
R_earth=6400000;
R=8.31447;

T=T_SL-L*H;
g_h=g*(R_earth/(R_earth+H))^2;
P=P_SL*(1-(L*H)/T_SL)^((g_h*M_density)/(R*L));
density_loiter=((P*M_density)/(R*T))*.00194;


%_______________ Density for Max velocity altitude________________________


H=h_max_velocity/3.28;
P_SL=101325;
T_SL=288.15;
g=9.80665;
L=0.0065;
M_density=0.0289645;
R_earth=6400000;
R=8.31447;

T=T_SL-L*H;
g_h=g*(R_earth/(R_earth+H))^2;
P=P_SL*(1-(L*H)/T_SL)^((g_h*M_density)/(R*L));
density_max_velocity=((P*M_density)/(R*T))*.00194;



% ____________________Valocity convertion________________________________

V_stall=((Stall_velocity*1000)/3600)*3.28;                  % Stall velocity in ft/s
V_cruise=((Cruise_velocity*1000)/3600)*3.28;                % Cruise velocity in ft/s
V_loiter=((Loiter_velocity*1000)/3600)*3.28;
V_max=((Maximum_velocity*1000)/3600)*3.28;


    
%__________________ Wing Loading Calculation_____________________________

% Wing loading for take off

W_S_to=(hp_w*take_off_dist*CL_wing_take_off*density*g_ground)/1.44;

% Wing loading fo cruise 

C_Do=Swet_Sref*Cfe;
q_cruise=0.5*density_cruise*(V_cruise^2);

W_S_cruise=(q_cruise*(3.1416*AR_wing*e1*C_Do)^0.5)/.90;

% Wing loading for loiter

q_loiter=density_loiter*0.5*(V_loiter^2);
W_S_loiter=(q_loiter*((3*3.1416*AR_wing*e1*C_Do)^0.5))/0.90;


% Wing loading for V_stall

W_S_stall=0.5*density*((V_stall)^2)*CL_wing_landing;

W_S_selected=W_S_loiter;

%________________Calculations for aircraft geometry_______________________

area=weight_aircraft/W_S_selected;      % wing area
b_w_r=((AR_wing*area)^(1/2))/2;         % wing half span
b_w_l=-b_w_r;
cr_w=(area)/((1+TR)*b_w_r);             % wing root chord
ct_w=TR*cr_w;                           % wing tip chord
mac_w=(0.666667*cr_w*(1+TR+TR^2))/(1+TR);   % Wing MAC
mac_w_yy=(b_w_r*(1+2*TR))/(3*(1+TR));   % Wing MAC distance from root chord
mac_w_difr_x=mac_w_yy*tand(angle);      % Distnace between cr_w/4 to mac/4
mac_w_xx=mac_w_difr_x+(cr_w/4);         % Distance of Wing MAC/4 from LE

%___________________ Calculation for Neutral Point________________________

A_w=(AR_wing*Ao_w)/(AR_wing+18.5*Ao_w);
A_ht=(AR_HT*Ao_ht)/(AR_HT+18.5*Ao_ht);

np=((mac_w_xx)+(Ns*V_HT*(A_ht/A_w)*(1-down_wash)));

np_x=np+ref_point;
np_y=0;
np_z=0;

st_marg=static_margin/100; 
c_g=mac_w*(40/100);                     % CG of wing
dist_x=(cr_w/4+mac_w_difr_x)-mac_w/4;   % Distance btwn mac_w 1st point to cr_w 1st point
x_dist_cg=np-(st_marg*mac_w);           % CG of Aircraft by static margin
c_g_position=ref_point+x_dist_cg;                          
difr=b_w_r*tand(angle);

a=cr_w+ref_point;
b=cr_w/4+(3*ct_w/4)+difr+ref_point;
c=cr_w/4-ct_w/4+difr+ref_point;
d=ref_point;

z_difr_r_a=0;
z_difr_r_b=b_w_r*tand(dihy_angle);
z_difr_r_c=b_w_r*tand(dihy_angle);
z_difr_r_d=0;

z_difr_mac_a=mac_w_yy*tand(dihy_angle);
z_difr_mac_b=mac_w_yy*tand(dihy_angle);

% Aerodynamic Center of MAC

ac_mac_x=mac_w_xx+ref_point;
ac_mac_y=0;
ac_mac_z=0;

cg_aircraft_x=ref_point+x_dist_cg;
cg_aircraft_y=0;
cg_aircraft_z=0;

% Output Results For wing

span=2*b_w_r;
root_w=cr_w*12;
tip_w=ct_w*12;
ac_w=(cr_w/4+mac_w_difr_x)*12;
CG_aircraft=x_dist_cg*12;
MAC_w=mac_w*12;
NP_wing=np*12;

% _____________________Calculations for Tail_____________________________

area_HT = (V_HT*mac_w*area)/arm;        % H-tail area 
area_VT = (V_VT*2*b_w_r*area)/arm;      % V-tail area
b_ht_r=((area_HT*AR_HT)^(1/2))/2;       % H-tail half span
b_ht_l=-b_ht_r;
b_vt=((area_VT*AR_VT)^(1/2));           % V-tail span
cr_ht=(2*area_HT)/((1+TR_HT)*2*b_ht_r); % H-tail root chord
ct_ht=TR_HT*cr_ht;                      % H-tail tip chord
cr_vt=(2*area_VT)/((1+TR_VT)*b_vt);     % V-tail root chord
ct_vt=TR_VT*cr_vt;                      % V-tail tip chord

mac_ht=(2*cr_ht*(1+TR_HT+TR_HT^2))/(3*(1+TR_HT)); % H-tail MAC
mac_vt=(2*cr_vt*(1+TR_VT+TR_VT^2))/(3*(1+TR_VT)); % V-tail MAC

mac_ht_yy=(b_ht_r*(1+2*TR_HT))/(3*(1+TR_HT));   % Distacne of H-tail MAC from H-tail root chord
mac_vt_zz=(b_vt*(1+2*TR_VT))/(6*(1+TR_VT));     % Distacne of V-tail MAC from V-tail root chord

mac_ht_difr_x=mac_ht_yy*tand(angle_ht);
mac_vt_difr_x=mac_vt_zz*tand(angle_vt);

dist_ht_x=((cr_ht/4)+mac_ht_difr_x)-mac_ht/4;
dist_vt_x=((cr_vt/4)+mac_vt_difr_x)-mac_vt/4;


difr_mac_ht=(cr_ht/4)-(mac_ht/4);
difr_mac_vt=(cr_vt/4)-(mac_vt/4);

arm_length=arm+x_dist_cg+ref_point;
arm_length_ht=ref_point+x_dist_cg+arm;     % Distance from wing LE to H-tail mac/4 
arm_length_vt=ref_point+x_dist_cg+arm;     % Distance from wing LE to v-tail mac/4 


% MAC of H-tail

mac_ht_position_x=[arm_length_ht+dist_ht_x-(cr_ht/4) arm_length_ht+dist_ht_x+((3*mac_ht)/4)];
mac_ht_position_y=[mac_ht_yy mac_ht_yy];
mac_ht_position_z=[0 0];

difr_ht=b_ht_r*tand(angle_ht);
difr_vt=b_vt*tand(angle_vt);

del_x=cr_ht-ct_ht;                      % H-tail root chord-H-tail tip chord
del_x_vt=cr_vt-ct_vt;


a_ht=arm_length_ht+(3*(cr_ht/4));
b_ht=arm_length_ht+(3*(ct_ht/4))+difr_ht;
c_ht=arm_length_ht-(ct_ht/4)+difr_ht;                         
d_ht=arm_length_ht-(cr_ht/4);                          

a_vt=arm_length_vt+(3*(cr_vt/4));
b_b_vt=arm_length_vt+(3*(ct_vt/4))+difr_vt;
c_vt=arm_length_vt-(cr_vt/4)+difr_vt;
d_vt=arm_length_vt-(cr_vt/4);

% Output Results For Horizontal Tail

span_HT=2*b_ht_r;
root_HT=cr_ht*12;
tip_HT=ct_ht*12;
MAC_HT=mac_ht*12;

% Output Results For Vartical Tail

span_VT=b_vt;
root_VT=cr_vt*12;
tip_VT=ct_vt*12;
MAC_VT=mac_vt*12;


% ____________________________________________Fuselage Drawing___________

length_aircraft=ref_point+mac_w_xx+arm+(3*cr_ht/4);
width_fuselage=length_aircraft/fine_ness_ratio;


f_x1=[0 arm_length_ht+(3*(cr_ht/4))];
f_y1=[0 0];
f_z1=[0 0];

f_x_r=[nose cr_w+ref_point+nose];
f_y_r=[width_fuselage/2 width_fuselage/2];
f_z_r=[0 0];

f_x_l=[nose cr_w+ref_point+nose];
f_y_l=[-width_fuselage/2 -width_fuselage/2];
f_z_l=[0 0];

f_tail_x_r=[cr_w+ref_point+nose arm_length_ht+(3*(cr_ht/4))];
f_tail_y_r=[width_fuselage/2 0];
f_tail_z_r=[0 0];

f_tail_x_l=[cr_w+ref_point+nose arm_length_ht+(3*(cr_ht/4))];
f_tail_y_l=[-width_fuselage/2 0];
f_tail_z_l=[0 0];

f_x_rr=[0 nose];
f_y_rr=[0 width_fuselage/2];
f_z_rr=[0 0];

f_x_ll=[0 nose];
f_y_ll=[0 -width_fuselage/2];
f_z_ll=[0 0];

% % c/4 Chord wing Sweep angle

c_4_x1=[cr_w/4+ref_point cr_w/4+difr+ref_point];
c_4_y1=[0 b_w_r];
c_4_z1=[0 z_difr_r_b];

c_4_x2=[cr_w/4+ref_point cr_w/4+difr+ref_point];
c_4_y2=[0 b_w_l];
c_4_z2=[0 z_difr_r_b];

% % c/4 Chord H-tail Sweep angle

c_4_ht_x1=[arm_length difr_ht+arm_length];
c_4_ht_y1=[0 b_ht_r];
c_4_ht_z1=[0 0];

c_4_ht_x2=[arm_length difr_ht+arm_length];
c_4_ht_y2=[0 b_ht_l];
c_4_ht_z2=[0 0];


% wing

x1 = [a b c d]; 
y1 = [0 b_w_r b_w_r 0]; 
z1 = [z_difr_r_a z_difr_r_b z_difr_r_c z_difr_r_d];

x2 = [a b c d]; 
y2 = [0 b_w_l b_w_l 0]; 
z2 = [z_difr_r_a z_difr_r_b z_difr_r_c z_difr_r_d];


% Flap design

c_flp=chord_flp/100;
c_elrn=chord_aileron/100;
spn_flp=b_w_r*(spn_of_flp/100);

chord_flap=c_flp*12;
span_flap=spn_flp*12;

angle_1=atand((ref_point+cr_w-b)/b_w_r);

d1=((width_fuselage/2)+0.1)*tand(angle_1);
d2=((width_fuselage/2)+0.1+spn_flp)*tand(angle_1);


a1_r_flap=ref_point+cr_w-d1;
a2_r_flap=ref_point+cr_w-(cr_w*c_flp)-d1;
a3_r_flap=ref_point+cr_w-(cr_w*c_elrn)-d2;
a4_r_flap=ref_point+cr_w-d2;

b1_r_flap=(width_fuselage/2)+0.1;
b2_r_flap=(width_fuselage/2)+0.1+spn_flp;

a1_l_flap=ref_point+cr_w-d1;
a2_l_flap=ref_point+cr_w-(cr_w*c_flp)-d1;
a3_l_flap=ref_point+cr_w-(cr_w*c_elrn)-d2;
a4_l_flap=ref_point+cr_w-d2;

b1_l_flap=-((width_fuselage/2)+0.1);
b2_l_flap=-((width_fuselage/2)+0.1+spn_flp);

z_difr_flp_a=b1_r_flap*tand(dihy_angle);
z_difr_flp_b=b1_r_flap*tand(dihy_angle);
z_difr_flp_c=b2_r_flap*tand(dihy_angle);
z_difr_flp_d=b2_r_flap*tand(dihy_angle);


x1_r_flap=[a1_r_flap a2_r_flap  a3_r_flap a4_r_flap];
y1_r_flap=[b1_r_flap b1_r_flap b2_r_flap b2_r_flap];
z1_r_flap=[z_difr_flp_a z_difr_flp_b z_difr_flp_c z_difr_flp_d];

x1_l_flap=[a1_l_flap a2_l_flap  a3_l_flap a4_l_flap];
y1_l_flap=[b1_l_flap b1_l_flap b2_l_flap b2_l_flap];
z1_l_flap=[z_difr_flp_a z_difr_flp_b z_difr_flp_c z_difr_flp_d];


% Aileron design

angle_1=atand((ref_point+cr_w-b)/b_w_r);

spn_elrn=b_w_r*(spn_of_aileron/100);

chord_eleron=c_elrn*12;
span_eleron=spn_elrn*12;

d3=((width_fuselage/2)+spn_flp+.2)*tand(angle_1);
d4=((width_fuselage/2)+spn_flp+spn_elrn+.2)*tand(angle_1);

a1_r_elrn=ref_point+cr_w-d3;
a2_r_elrn=ref_point+cr_w-(cr_w*c_elrn)-d3;
a3_r_elrn=ref_point+cr_w-(ct_w*c_elrn)-d4;
a4_r_elrn=ref_point+cr_w-d4;

b1_r_elrn=(width_fuselage/2)+0.1+spn_flp+.1;
b2_r_elrn=(width_fuselage/2)+0.1+spn_flp+spn_elrn+.1;

a1_l_elrn=ref_point+cr_w-d3;
a2_l_elrn=ref_point+cr_w-(cr_w*c_elrn)-d3;
a3_l_elrn=ref_point+cr_w-(ct_w*c_elrn)-d4;
a4_l_elrn=ref_point+cr_w-d4;

b1_l_elrn=-((width_fuselage/2)+0.1+spn_flp+.1);
b2_l_elrn=-((width_fuselage/2)+0.1+spn_flp+spn_elrn+.1);


z_difr_elrn_a=b1_r_elrn*tand(dihy_angle);
z_difr_elrn_b=b1_r_elrn*tand(dihy_angle);
z_difr_elrn_c=b2_r_elrn*tand(dihy_angle);
z_difr_elrn_d=b2_r_elrn*tand(dihy_angle);

x1_r_elrn=[a1_r_elrn a2_r_elrn  a3_r_elrn a4_r_elrn];
y1_r_elrn=[b1_r_elrn b1_r_elrn b2_r_elrn b2_r_elrn];
z1_r_elrn=[z_difr_elrn_a z_difr_elrn_b z_difr_elrn_c z_difr_elrn_d];

x1_l_elrn=[a1_l_elrn a2_l_elrn  a3_l_elrn a4_l_elrn];
y1_l_elrn=[b1_l_elrn b1_l_elrn b2_l_elrn b2_l_elrn];
z1_l_elrn=[z_difr_elrn_a z_difr_elrn_b z_difr_elrn_c z_difr_elrn_d];


% ELevator design

c_ele=chord_ele/100;
spn_ele=b_ht_r*(spn_of_elevator/100);

span_elevator=spn_ele*12;
chord_elevator=c_ele*12;

angle_1=atand((a_ht-b_ht)/b_ht_r);

d1=(.1+0.1)*tand(angle_1);
d2=((.1)+0.1+spn_ele)*tand(angle_1);

a1_r_ele=a_ht-d1;
a2_r_ele=a_ht-(cr_ht*c_ele)-d1;
a3_r_ele=a_ht-(ct_ht*c_ele)-d2;
a4_r_ele=a_ht-d2;

b1_r_ele=.1+0.1;
b2_r_ele=.1+0.1+spn_ele;

a1_l_ele=a_ht-d1;
a2_l_ele=a_ht-(cr_ht*c_ele)-d1;
a3_l_ele=a_ht-(ct_ht*c_ele)-d2;
a4_l_ele=a_ht-d2;

b1_l_ele=-(.1+0.1);
b2_l_ele=-(.1+0.1+spn_ele);


x1_r_ele=[a1_r_ele a2_r_ele  a3_r_ele a4_r_ele];
y1_r_ele=[b1_r_ele b1_r_ele b2_r_ele b2_r_ele];
z1_r_ele=[0 0 0 0];

x1_l_ele=[a1_l_ele a2_l_ele  a3_l_ele a4_l_ele];
y1_l_ele=[b1_l_ele b1_l_ele b2_l_ele b2_l_ele];
z1_l_ele=[0 0 0 0];



% MSC of wing

mac_w_x=[mac_w+dist_x+ref_point dist_x+ref_point];
mac_w_y=[mac_w_yy mac_w_yy];
mac_w_z=[z_difr_mac_a z_difr_mac_b];

% C.G position

c_g_x=c_g_position+ref_point;
c_g_y=0;
c_g_z=0;

% Horaizontal Tail

x3=[a_ht b_ht c_ht d_ht];
y3=[0 b_ht_r b_ht_r 0];
z3=[0 0 0 0];

x4=[a_ht b_ht c_ht d_ht];
y4=[0 b_ht_l b_ht_l 0];
z4=[0 0 0 0];


% Vartical Tail

x5=[a_vt b_b_vt c_vt d_vt];
y5=[0 0 0 0];
z5=[0 b_vt b_vt 0];

% Midline H-Tail

mac_t_x=[arm_length+(3*cr_ht/4) arm_length-(cr_ht/4)];
mac_t_y=[0 0];
mac_t_z=[0 0];


% Rudder

c_rud=chord_rud/100;
chord_rudder=c_rud*12;
span_rudder=b_vt*12;
rud_x1= [arm_length+(3*cr_vt/4)-(cr_vt*c_rud) arm_length_ht+(3*ct_vt/4)+difr_vt-(cr_vt*c_rud)];
rud_y1= [0 0];
rud_z1= [0 b_vt];

%_______________________________Power Required in HP____________________

% Power Required for Take off

radi_flt_path=(6.96*(1.15*V_stall)^2)/g;
flt_path_angle=acosd(1-(h_o/radi_flt_path));
air_borne_dist=radi_flt_path*sind(flt_path_angle);
ground_roll=take_off_dist-air_borne_dist;
q_take_off=0.5*density*(0.77*V_stall)^2;


L_D_take_off=1/(((q_take_off*C_Do)/W_S_selected)+((W_S_selected/(q_take_off*3.1416*AR_wing*e1))));

thrust_weight_TO=(1.21*W_S_selected)/(g*density*CL_wing_take_off*ground_roll);

P_take_off=((thrust_weight_TO*weight_aircraft*0.77*V_stall)/0.85)/550;

% Power required for cruise


W_S_cruise_Avg=W_S_selected*.90;
L_D_cruise=1/(((q_cruise*C_Do)/W_S_cruise_Avg)+((W_S_cruise_Avg/(q_cruise*3.1416*AR_wing*e1))));
K=1/(4*C_Do*(L_D_cruise^2));

T_W_cruise_Avg=((q_cruise*C_Do)/W_S_cruise_Avg)+((W_S_cruise_Avg)/(q_cruise*3.1416*AR_wing*e1));

P_cruise_SL=((T_W_cruise_Avg*W_S_cruise_Avg*area*V_cruise)/.85)/550;
P_cruise=P_cruise_SL*((1.132*(density_cruise/density))-0.132); % Altitude carection

% Power required for Loiter

% q_loiter=0.5*density_loiter*(V_loiter^2);
L_D_loiter=1/(((q_loiter*C_Do)/W_S_loiter)+((W_S_loiter/(q_loiter*3.1416*AR_wing*e1))));
K_loiter=1/(4*C_Do*(L_D_loiter^2));

T_W_loiter=((q_loiter*C_Do)/W_S_selected)+((W_S_selected)/(q_loiter*3.1416*AR_wing*e1));

P_loiter_SL=((T_W_loiter*W_S_loiter*area*V_loiter)/.85)/550;
P_loiter=P_loiter_SL*((1.132*(density_loiter/density))-0.132); % Altitude carection

% Power required for maximum velocity

q_max_velocity=0.5*density_max_velocity*V_max^2;

L_D_max_velocity=1/(((q_max_velocity*C_Do)/W_S_cruise_Avg)+((W_S_cruise_Avg/(q_max_velocity*3.1416*AR_wing*e1))));
K_max_velocity=1/(4*C_Do*(L_D_max_velocity^2));

T_W_max_velocity=((q_max_velocity*C_Do)/W_S_cruise_Avg)+((W_S_cruise_Avg)/(q_max_velocity*3.1416*AR_wing*e1));

P_max_velocity_SL=((T_W_max_velocity*W_S_cruise_Avg*area*V_max)/.85)/550;        % shaft power at SL
P_max_velocity=P_max_velocity_SL*((1.132*(density_max_velocity/density))-0.132); % Altitude carection



% __________________________Thrust Required in lb_________________________

% Thrust Required for Take off
thrust_take_off=(P_take_off*.85*550)/(0.77*V_stall);
% Thrust Required for Cruise
thrust_cruise=(P_cruise*.85*550)/(V_cruise);
% Thrust Required for Maximum Velocity
thrust_max_velocity=(P_max_velocity*.85*550)/(V_max);
% Thrust Required for Loiter
thrust_loiter=(P_loiter*.85*550)/(V_loiter);


%___________________________Power Required in Watts______________________

P_take_off_watt=P_take_off*745.69;
P_cruise_watt=P_cruise*745.69;
P_max_velocity_watt=P_max_velocity*745.69;
P_loiter_watt=P_loiter*745.69;
%___________________________________ Parasite Drag________________________


gama=1.4;
row=density_cruise;
a=(((gama*R*T)/M_density)^0.5)*3.28; % Speed of sound

v=05:5:365;

meu=3.79*10^(-7);
S_ref=area;
S=S_ref;
W_S=W_S_loiter;

for i=1:73
M(i)=(v(i)/a);

% wing
Re_w(i)=(row*v(i)*mac_w)/meu;
cf_t_w(i)=.455/(((log10(Re_w(i)))^2.58)*(1+.0144*M(i).^2)^0.65);
FF_w=1.245;
Q_w=1;
S_wet_w=2.003*area-(width_fuselage*cr_w);

CDo_wing(i)=(cf_t_w(i)*FF_w*Q_w*S_wet_w);

% Fuselage

Re_f(i)=(row*v(i)*length_aircraft)/meu;
cf_t_f(i)=.455/(((log10(Re_f(i)))^2.58)*(1+.0144*M(i).^2)^0.65);
FF_f=1.098;
Q_f=1;
S_wet_f=3.4*((length_aircraft*width_fuselage)-(length_aircraft*.45*width_fuselage*.5));

CDo_fuselage(i)=(cf_t_f(i)*FF_f*Q_f*S_wet_f);

%Horizontal Tail

Re_HT(i)=(row*v(i)*mac_ht)/meu;
cf_t_HT(i)=.455/(((log10(Re_HT(i)))^2.58)*(1+.0144*M(i).^2)^0.65);
FF_HT=1.26;
Q_HT=1;
S_wet_HT=2.003*area_HT;

CDo_HT(i)=(cf_t_HT(i)*FF_HT*Q_HT*S_wet_HT);

%Vartical Tail

Re_VT(i)=(row*v(i)*mac_vt)/meu;
cf_t_VT(i)=.455/(((log10(Re_VT(i)))^2.58)*(1+.0144*M(i).^2)^0.65);
FF_VT=1.2;
Q_VT=1;
S_wet_VT=2.003*area_VT;

CDo_VT(i)=(cf_t_VT(i)*FF_VT*Q_VT*S_wet_VT);

% Miscellaneous Drag
CDo_misc=.00197;

CL(i)=(2*W_S)/(row*(v(i).^2));

% Total Parasite Drag

CD_all(i)=CDo_wing(i)+CDo_fuselage(i)+CDo_HT(i)+CDo_VT(i);
CDo(i)=(CD_all(i)/S_ref)+CDo_misc;
    

CD(i)=CDo(i)+K*CL(i)^2;
Tr(i)=(row*(v(i).^2)*S*CD(i))/2;
CL_CD(i)=CL(i)/CD(i);
v(i)=M(i)*a;


end


% _________________________________Ploting Wing Geometry___________________

figure(1)


plot3(x1,y1,z1,'b',x2,y2,z2,'b',c_4_x1,c_4_y1,c_4_z1,'r',c_4_x2,c_4_y2,c_4_z2,'r',...
x3,y3,z3,'b',x4,y4,z4,'b',...
x5,y5,z5,'b',rud_x1,rud_y1,rud_z1,'k',...
mac_w_x,mac_w_y,mac_w_z,'r'); 
title('Aircraft Geomrtry')
hold on

plot3(c_4_ht_x1,c_4_ht_y1,c_4_ht_z1,'r',c_4_ht_x2,c_4_ht_y2,c_4_ht_z2,'r');
hold on

% ploting flap

plot3(x1_r_flap,y1_r_flap,z1_r_flap,'k',x1_l_flap,y1_l_flap,z1_l_flap,'k');
hold on

% Ploting Eleron

plot3(x1_r_elrn,y1_r_elrn,z1_r_elrn,'k',x1_l_elrn,y1_l_elrn,z1_l_elrn,'k');
hold on

% Ploting Elevator

plot3(x1_r_ele,y1_r_ele,z1_r_ele,'k',x1_l_ele,y1_l_ele,z1_l_ele,'k');
hold on

% polting C.G

plot3(cg_aircraft_x,cg_aircraft_y,cg_aircraft_z,'g*',ac_mac_x,ac_mac_y,...
    ac_mac_z,'b*',np_x,np_y,np_z,'r*');
hold on
plot3(mac_ht_position_x,mac_ht_position_y,mac_ht_position_z,'r');
hold on

plot3(f_x1,f_y1,f_z1,'b-',f_x_r,f_y_r,f_z_r,f_x_l,f_y_l,f_z_l,'b-',...
    f_tail_x_r,f_tail_y_r,f_tail_z_r,'b-',f_tail_x_l,f_tail_y_l,f_tail_z_l,'b-',...
    f_x_rr,f_y_rr,f_z_rr,'b',f_x_ll,f_y_ll,f_z_ll,'b');
hold on

daspect([1,1,1])                        % Equal size of all axis
axis([0 b_ht+1 -2+b_w_l 2+b_w_r -2 2+b_vt]);
axis on



%___________________________ Publishing Results__________________________



figure(2)
axis off
% axis([0 2.5 0 1])
text(0,1,'Calculated Result')

text(0,.90,'wing Parameter');               
text(0,.85,'Wing Loading ');               text(0.3,0.85,num2str(W_S_selected));
text(0,.80,'Surface Area,ft^2 ');          text(0.3,0.8,num2str(area));                                        
text(0,.75,'span,ft           ');          text(0.3,0.75,num2str(span));	
text(0,.70,'Root chord,in     ');          text(0.3,0.7,num2str(root_w));
text(0,.65,'Tip chord,in      ');          text(0.3,0.65,num2str(tip_w));  
text(0,.60,'MAC,in            ');          text(0.3,0.60,num2str(MAC_w));
text(0,.55,'AC from LE,in     ');          text(0.3,0.55,num2str(ac_w));
text(0,.50,'CG from LE,in     ');          text(0.3,0.50,num2str(CG_aircraft));
text(0,.45,'NP from LE,in     ');          text(0.3,0.45,num2str(NP_wing));

text(0,.35,'Length Of Aircraft,ft');       text(0.3,0.35,num2str(length_aircraft));
text(0,.30,'Width Of Aircraft,ft');        text(0.3,0.30,num2str(width_fuselage));
text(0,.25,'Span of Flap,in');             text(0.3,0.25,num2str(span_flap));
text(0,.20,'Chord of Flap,in');            text(0.3,0.20,num2str(chord_flap));
text(0,.15,'Span of Eleron,in');           text(0.3,0.15,num2str(span_eleron));
text(0,.10,'Chord of Eleron,in');          text(0.3,0.10,num2str(chord_eleron));

text(0,.05,'Power for take off,HP');       text(0.3,0.05,num2str(P_take_off));

text(0.5,.90,'Horizontal Tail Parameter');               
                                      
text(0.5,.85,'Surface Area,ft^2 ');          text(0.8,0.85,num2str(area_HT));                                        
text(0.5,.80,'span,ft           ');          text(0.8,0.80,num2str(span_HT));	
text(0.5,.75,'Root chord,in     ');          text(0.8,0.75,num2str(root_HT));
text(0.5,.70,'Tip chord,in      ');          text(0.8,0.70,num2str(tip_HT));  
text(0.5,.65,'MAC,in            ');          text(0.8,0.65,num2str(MAC_HT));
text(0.5,.60,'Span of Elevator,in');         text(0.8,0.60,num2str(span_elevator));  
text(0.5,.55,'Chord of Elevator,in');        text(0.8,0.55,num2str(chord_elevator));

text(0.5,.45,'Vartical Tail Parameter');               
                                      
text(0.5,.40,'Surface Area,ft^2 ');          text(0.8,0.40,num2str(area_VT));                                        
text(0.5,.35,'span,ft           ');          text(0.8,0.35,num2str(span_VT));	
text(0.5,.30,'Root chord,in     ');          text(0.8,0.30,num2str(root_VT));
text(0.5,.25,'Tip chord,in      ');          text(0.8,0.25,num2str(tip_VT));  
text(0.5,.20,'MAC,in            ');          text(0.8,0.20,num2str(MAC_VT));
text(0.5,.15,'Span of Rudder,in ');          text(0.8,0.15,num2str(span_rudder));  
text(0.5,.10,'Chord of Rudder,in');          text(0.8,0.10,num2str(chord_rudder));

figure(3)
axis off

text(0,1,'Calculated Parameters')

text(0,.90,'Wing Loading');               

text(0,.85,'Take off ');                    text(0.3,0.85,num2str(W_S_to));
text(0,.80,'Cruise');                       text(0.3,0.8,num2str(W_S_cruise));                                        
text(0,.75,'Loiter');                       text(0.3,0.75,num2str(W_S_loiter));	
text(0,.70,'Stall velocity');               text(0.3,0.7,num2str(W_S_stall));


text(0,.60,'Lift to Drag Ratio'); 

text(0,.55,'Take off');                     text(0.3,0.55,num2str(L_D_take_off));
text(0,.50,'Cruise');                       text(0.3,0.50,num2str(L_D_cruise));
text(0,.45,'Maximum Velocity');             text(0.3,0.45,num2str(L_D_max_velocity));
text(0,.40,'loiter');                       text(0.3,0.40,num2str(L_D_loiter));

text(0,.30,'Thrust to Weight ratio'); 

text(0,.25,'Take off');                     text(0.3,0.25,num2str(thrust_weight_TO));
text(0,.20,'Cruise');                       text(0.3,0.20,num2str(T_W_cruise_Avg));
text(0,.15,'Maximum Velocity');             text(0.3,0.15,num2str(T_W_max_velocity));
text(0,.10,'loiter');                       text(0.3,0.10,num2str(T_W_loiter));



text(0.5,.90,'Shaft Power Required ,HP');           

text(0.5,.85,'Take off');                     text(0.8,0.85,num2str(P_take_off));
text(0.5,.80,'Cruise');                       text(0.8,0.80,num2str(P_cruise));
text(0.5,.75,'Maximum Velocity');             text(0.8,0.75,num2str(P_max_velocity));
text(0.5,.70,'loiter');                       text(0.8,0.70,num2str(P_loiter));

text(0.5,.60,'Power Required ,Watts');           

text(0.5,.55,'Take off');                     text(0.8,0.55,num2str(P_take_off_watt));
text(0.5,.50,'Cruise');                       text(0.8,0.50,num2str(P_cruise_watt));
text(0.5,.45,'Maximum Velocity');             text(0.8,0.45,num2str(P_max_velocity_watt));
text(0.5,.40,'loiter');                       text(0.8,0.40,num2str(P_loiter_watt));


text(0.5,.30,'Thrust Required,lb'); 

text(0.5,.25,'Take off');                     text(0.8,0.25,num2str(thrust_take_off));
text(0.5,.20,'Cruise');                       text(0.8,0.20,num2str(thrust_cruise));
text(0.5,.15,'Maximum Velocity');             text(0.8,0.15,num2str(thrust_max_velocity));
text(0.5,.10,'loiter');                       text(0.8,0.10,num2str(thrust_loiter));



figure(4)
axis off
text(0,1,'In put conditions')

text(0,.90,'Design parameter');               
text(0,.85,'Weight of Aircraft,lb');       text(0.35,0.85,num2str(weight_aircraft));
text(0,.80,'Runway length,ft');            text(0.35,0.8,num2str(take_off_dist));                                        
text(0,.75,'Stall velocity,Km/h');         text(0.35,0.75,num2str(Stall_velocity));	
text(0,.70,'Cruise velocity,Km/h');        text(0.35,0.7,num2str(Cruise_velocity));
text(0,.65,'Maximum Velocity,Km/h');       text(0.35,0.65,num2str(Maximum_velocity));  
text(0,.60,'Loiter velocity,Km/h');        text(0.35,0.60,num2str(Loiter_velocity));
text(0,.55,'Cruise Altitude,ft');          text(0.35,0.55,num2str(h_cruise));
text(0,.50,'loiter Altitude,ft');          text(0.35,0.50,num2str(h_loiter));
text(0,.45,'Max Velocity Altitude,ft');    text(0.35,0.45,num2str(h_max_velocity));           
text(0,.40,'HP to weigth ratio');          text(0.35,0.40,num2str(hp_w));
text(0,.35,'Root Airfoil Max Cl');         text(0.35,0.35,num2str(cl_max_cr));                                        
text(0,.30,'Tip airfoil Max Cl');          text(0.35,0.30,num2str(cl_max_ct));	
text(0,.25,'Flap Dife Takeoff,deg');       text(0.35,0.25,num2str(flp_dfl_takeoff));
text(0,.20,'Flap Dife Landing,deg');       text(0.35,0.20,num2str(flp_dfl_land));  

text(0.45,1,'Aircraft Geometry');                                                    
text(0.45,.95,'Aspect Ratio wing ');                    text(0.95,0.95,num2str(AR_wing));                                        
text(0.45,.90,'Aspect Ratio Htail');                    text(0.95,0.90,num2str(AR_HT));	
text(0.45,.85,'Aspect Ratio Vtail');                    text(0.95,0.85,num2str(AR_VT));
text(0.45,.80,'Tapper Ratio wing');                     text(0.95,0.80,num2str(TR));  
text(0.45,.75,'Tapper Ratio Htail');                    text(0.95,0.75,num2str(TR_HT));
text(0.45,.70,'Tapper Ratio Vtail');                    text(0.95,0.70,num2str(TR_VT));  
text(0.45,.65,'Tail Arm lenght,ft');                    text(0.95,0.65,num2str(arm));                                                
text(0.45,.60,'Quarter Chord sweep wing,deg');          text(0.95,0.60,num2str(angle));                                        
text(0.45,.55,'Quarter Chord sweep Htail,deg');         text(0.95,0.55,num2str(angle_ht));	
text(0.45,.50,'Quarter Chord sweep Vtail,deg');         text(0.95,0.50,num2str(angle_vt));
text(0.45,.45,'Dihedral Angle,deg');                    text(0.95,0.45,num2str(dihy_angle));  
text(0.45,.40,'Htail Volume co-efficient');             text(0.95,0.40,num2str(V_HT));
text(0.45,.35,'Vtail Volume co-efficient');             text(0.95,0.35,num2str(V_VT));  
text(0.45,.30,'Flap chord % of wing chord');            text(0.95,0.30,num2str(chord_flp));
text(0.45,.25,'Aileron chord % of wing chord');         text(0.95,0.25,num2str(chord_aileron));
text(0.45,.20,'Elevator chord % of Htail chord');       text(0.95,0.20,num2str(chord_ele));
text(0.45,.15,'Rudder chord % of Vtail chord');         text(0.95,0.15,num2str(chord_rud));
text(0.45,.10,'Flap span % of half wing span,in');      text(0.95,0.10,num2str(spn_of_flp));
text(0.45,.05,'Aileron span % of half wing span,in');   text(0.95,0.05,num2str(spn_of_aileron));
text(0.45,0.0,'Elevator span % of Htail span,in');      text(0.95,0.0,num2str(spn_of_elevator));


figure(5)
plot(v,Tr)
title('Thrust Required in cruise')
xlabel('Velocity,ft/s')
ylabel('Thrust Tequired,lb')
grid on

figure(6)
plot(v,CL_CD)
title('Lift to Drag ratio in cruise')
xlabel(' Velocity,ft/s')
ylabel('Lift/Drag')
grid on

figure(7)
plot(v,CDo)
title('Parasite Drag in cruise')
xlabel('Velocity,ft/s')
ylabel('Parasite Drag')
grid on
